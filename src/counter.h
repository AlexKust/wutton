// Author: Lifelover
// Source: http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-shirokoveschatelnye-soobscheniya-i-dhcp.html
// License: WTFPL

#pragma once

#include <avr/interrupt.h>

extern uint32_t tick_count;
extern uint32_t second_count;

#define gettc()     tick_count
#define rtime()     second_count

void counter_init(void);

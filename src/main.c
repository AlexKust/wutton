// Wake up on LAN with Atmega8 and enc28j60.
// The interaction with enc28j60 is implemented by Lifelover (enc28j60.h, enc28j60.c, lan.h, lan.c, counter.h, counter.c).
// Not all functions from the Lifelover's code are used.

#include <ctype.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>
#include "lan.h"
#include "counter.h"

void udp_packet(eth_frame_t *frame, uint16_t len)
{
}


// MAC for waking up
const uint8_t mac PROGMEM = {1};

void indicateActiveMode(void)
{
    PORTA |= (1 << PA0);
}

void indicateInactiveMode(void)
{
    PORTA &= ~(1 << PA0);
}

int main(void)
{
    DDRA |= (1<<PA0);     // PA0 is for activity led
    PORTA |= (1<<PA0);

    DDRD &= ~(1 << PD2);  // Set PD2 as input (Using for interupt INT0)
    PORTD |= 1<<PD2;        // Enable PD2 pull-up resistor

    indicateActiveMode();
    _delay_ms(250);
    indicateInactiveMode();
    _delay_ms(250);
    
    sei();
    lan_init();
    
    indicateActiveMode();

    icmp_reset_ping_status();
    while( ! icmp_is_pong_received())
    {
        icmp_ping_gateway();
        _delay_ms(100);
        lan_poll();
    }
    
    wol();
    indicateInactiveMode();
}